# Open Deck Website #

A picture says 1000 words!

![Video of first prototype](https://bytebucket.org/opendecktechnologies/open-deck-website/raw/3473093ad7b6415857aefdddc758b2a2089b52c4/Resources/Local_host_video.gif?token=a8d472ab2d19b501621b8f4d1686de4c77da65b8)

### What is this repository for?

Open Deck is a website and native mobile app that will simulate a sort of open playground to play card games with a virtual deck of cards.

### What is this repository for?

This repo is for:

* Version control for the website
* Allowing collaboration and feedback

Enjoy!