var socket;

// PIXI CODE
var app = new PIXI.Application(),
	loader = PIXI.loader,
	resources = PIXI.loader.resources,
	Sprite = PIXI.Sprite,
	Rectangle = PIXI.RoundedRectangle,
	TextureCache = PIXI.utils.TextureCache,
	Texture = PIXI.Texture,
	Text = PIXI.Text,
	Graphics = PIXI.Graphics,
	Container = PIXI.Container,
	animationId,
	currentSprite,
	playerContainers = [],
	shuffledDeck = [];

var type = "WebGL";
if (!PIXI.utils.isWebGLSupported()) {
	type = "canvas";
}

// ADD ITEMS ONCE DOCUMENT IS READY
document.addEventListener("DOMContentLoaded", function(event) {
	app.renderer.backgroundColor = 0x008800;
	app.renderer.view.style.position = "absolute";
	app.renderer.view.style.display = "block";
	app.renderer.autoResize = true;
	app.renderer.resize(window.innerWidth, window.innerHeight);
	document.body.appendChild(app.view);
	loader
		.add("img/cards/tileset_flip.png")
		.add("img/cards/tileset.png")
		.add("img/player128.png")
		.load(socketSetup);
});

function socketSetup() {
	// ADD PLAYER TO SOCKET AND LISTEN FOR OTHERS
	socket = io.connect(window.location.href, {
		query:
			'windowwidth=' + window.innerWidth + '&' +
			'windowheight=' + window.innerHeight
	});
	socket.on("connect", function() {
		socket.on("addPlayers", addPlayers);
		socket.on("itemMoved", movingItem);
		socket.on("itemClicked", itemClicked);
		socket.on("clientDisconnected", disconnected);
		socket.on("setCardLocked", setCardLocked);
	});
}

function disconnected(data) {
	var spriteToRemove = app.stage.getChildByName(
		"player_container_" + data.socketId
	);
	playerContainers.splice(playerContainers.indexOf(spriteToRemove), 1);
	app.stage.removeChild(spriteToRemove);
}

function addPlayers(data) {
	console.log('deck...', data);

	// CREATE DECK
	if (!shuffledDeck.length) createCards(data);

	// ADD PLAYER SPRITES
	for (var i = data.sockets.length - 1; i >= 0; i--) {
		var currentSocketId = data.sockets[i];
		var onScreen = app.stage.getChildByName(
			"player_container_" + currentSocketId
		);

		if (!onScreen) {
			var container = new Container();
			container.id = currentSocketId;
			container.name = "player_container_" + currentSocketId;
			container.locked = false;
			container.width = 200;
			container.height = 200;
			container.pivot.x = 0.5;
			container.pivot.y = 0.5;

			var player = new Sprite(TextureCache["img/player128.png"]);
			player.name = "player_" + currentSocketId;
			player.anchor.x = 0.5;
			player.anchor.y = 0.5;
			container.addChild(player);

			// SHOW PLAYER'S ID
			var alias = "ID: " + currentSocketId.slice(0, 4);
			if (currentSocketId === socket.id) alias += "\n(you)";
			var playerName = new Text(alias);
			playerName.name = "player_alias_" + currentSocketId;
			playerName.style = { fontSize: 14, align: "center" };
			playerName.anchor.x = 0.5;
			playerName.anchor.y = 0.5;
			playerName.y = 30;
			player.addChild(playerName);

			// DEFINE PLAYER CARD BOUNDARY
			var circle = new Graphics();
			circle.name = "player_circle_" + currentSocketId;
			circle.lineStyle(2, 0xffffff);
			circle.drawCircle(player.x, player.y, 100);
			circle.endFill();
			circle.visible = false;
			container.addChild(circle);

			container.interactive = true;
			createDragAndDropFor(container);
			playerContainers.push(container);

			// // GET ALL PLAYERS POSITIONS
			// for (var i = 0; i < data.players.length; i++) {
			// 	console.log('players... ', data.players[i])
			// 	if (data.players[i].id == currentSocketId) {
			// 		container.x = data.players[i].xAxisValue;
			// 		container.y = data.players[i].yAxisValue;
			// 	}
			// };

			// ADD TO STAGE
			app.stage.addChild(container);
		}
	}

	// GET ALL PLAYERS POSITIONS
	for (var i = 0; i < data.players.length; i++) {
		// GET ON SCREEN SPRITE
		var playerSprite = app.stage.getChildByName("player_container_" + data.players[i].id);

		// APPLY SERVER VALUE TO SPRITE
		if (playerSprite) {
			playerSprite.x = data.players[i].xAxisValue;
			playerSprite.y = data.players[i].yAxisValue;
		}
	}

}

function createCards(data) {
	var offsetX = 0;
	var	offsetY = 0;
	var	width = 47.69;
	var	height = 64;
	var tileset = TextureCache["img/cards/tileset.png"];
	var	flippedTileset = TextureCache["img/cards/tileset_flip.png"];
	var tilesetWidth = 620;
	var tilesetHeight = 256;
	var	deck = [];

	// ITERATE THROUGH CARD DECK TILESET
	for (var i = 0; i < 52; i++) {
		var shuffledCoordinate = data.deck[i];

		// CREATE BACK OF CARD TEXTURE
		var backOfCard = Texture.fromImage("img/cards/card.png");
		var currentCard = new Sprite(backOfCard);
		currentCard.backTexture = backOfCard;

		// CREATE (FLIPPED) FRONT OF CARD TEXTURE
		var flippedRectangleMask = new Rectangle(
			shuffledCoordinate.offsetX,
			shuffledCoordinate.offsetY,
			width,
			height,
			1
		);
		flippedTileset.frame = flippedRectangleMask;
		var frontOfCardFlipped = new Texture(flippedTileset, flippedRectangleMask);
		currentCard.frontTextureFlipped = frontOfCardFlipped;
		
		currentCard.flipped = data.deck[i].flipped;
		currentCard.name = "card_" + data.deck[i].id;
		currentCard.id = socket.id;
		currentCard.locked = false;
		currentCard.interactive = true;
		currentCard.anchor.x = 0.5;
		currentCard.anchor.y = 0.5;
		currentCard.vx = 4.8;
		currentCard.scaleAnimation = 0.2;
		currentCard.shouldChangeTexture = true;

		createDragAndDropFor(currentCard);
		createClickListener(currentCard);

		deck.push(currentCard);
	}

	// DISPLAY AS A PACK OVERLAPPING IN MIDDLE OF SCREEN
	for (var i = 0; i < data.deck.length; i++) {
		var shuffledCard = deck[i];
		shuffledCard.x = data.deck[i].xAxisValue;		
		shuffledCard.y = data.deck[i].yAxisValue;
		app.stage.addChild(shuffledCard);
	}

	shuffledDeck = deck;

	// FLIPPING AFTER ITEMS HAVE BEEN ADDED TO STAGE
	data.deck.forEach(function(card, index) {
		if (!card.flipped) {
			currentSprite = app.stage.getChildByName("card_" + card.id);
			currentSprite.texture = currentSprite.frontTextureFlipped;
			currentSprite.scale.x = -1;
		}
	});
}

function flipToFront(sprite) {
	if (currentSprite.scale.x <= -1) {
		currentSprite.flipped = false;
		currentSprite.shouldChangeTexture = true;
		cancelAnimationFrame(animationId);
		currentSprite.scale.x = -1;
		return;
	}

	// CHANGE TEXTURE AT ZERO CROSSING
	if (currentSprite.shouldChangeTexture && currentSprite.scale.x < 0) {
		currentSprite.texture = currentSprite.frontTextureFlipped;
		currentSprite.shouldChangeTexture = false;
	}

	currentSprite.scale.x -= currentSprite.scaleAnimation;

	animationId = requestAnimationFrame(flipToFront);
}

function flipToBack(sprite) {
	currentSprite.scale.x += currentSprite.scaleAnimation;
	animationId = requestAnimationFrame(flipToBack);

	if (currentSprite.scale.x >= 1) {
		currentSprite.flipped = true;
		currentSprite.shouldChangeTexture = true;
		cancelAnimationFrame(animationId);
		currentSprite.scale.x = 1;
		return;
	}

	// CHANGE TEXTURE AT ZERO CROSSING
	if (currentSprite.shouldChangeTexture && currentSprite.scale.x > 0) {
		currentSprite.texture = currentSprite.backTexture;
		currentSprite.shouldChangeTexture = false;
	}
}

function createDragAndDropFor(target) {
	this.dragging = false;

	target.on("mousedown", onDragStart);
	target.on("touchstart", onDragStart);
	target.on("mousemove", onDragMove);
	target.on("touchmove", onDragMove);
	target.on("mouseup", onDragEnd);
	target.on("mouseupoutside", onDragEnd);
	target.on("touchend", onDragEnd);
	target.on("touchendoutside", onDragEnd);

	function onDragStart(event) {
		if (this.locked) return;

		this.data = event.data;
		this.alpha = 0.7;
		this.dragging = true;
		this.parent.addChild(this);

		if (playerContainers.indexOf(this) === -1) {
			for (var i = playerContainers.length - 1; i >= 0; i--) {
				var children = playerContainers[i].children;
				children[1].visible = true;
			}
		}
	}

	function onDragMove(event) {
		if (this.locked) return;
		if (this.dragging) {
			var newPosition = this.data.getLocalPosition(this);
			this.x = event.data.global.x;
			this.y = event.data.global.y;

			var newPosition = this.data.getLocalPosition(this);
			this.centerX += event.data.originalEvent.movementX;
			this.centerY += event.data.originalEvent.movementY;
		}
	}

	function onDragEnd(event) {
		if (this.locked) return;
		this.alpha = 1;
		this.dragging = false;
		this.data = event.data;

		// EMIT NEW POSITION
		var data = {
			id: target.name,
			coordinate: this.data.getLocalPosition(this.parent)
		};

		// BROADCAST ITEM HAS BEEN MOVED
		socket.emit("itemMoved", data);

		// CHECK ITEM BEING MOVED IS A CARD
		if (playerContainers.indexOf(this) === -1) {
			data.shouldLock = false;
			for (var i = playerContainers.length - 1; i >= 0; i--) {
				var playerContainer = playerContainers[i];

				// CHECK CARD HAS HIT A PLAYER
				var setCardLocked = hitTestRectangle(this, playerContainer);
				if (setCardLocked) {
					data.playerId = playerContainer.id;
					data.shouldLock = true;
					// socket.emit("setCardLocked", data);
				}
			}

			socket.emit("setCardLocked", data);
		}

		// REMOVE CIRCLE DROP ZONES
		for (var i = playerContainers.length - 1; i >= 0; i--) {
			playerContainers[i].children[1].visible = false;
		}
	}
}

function setCardLocked(data) {
	// LOCK THE CARD IF IT'S NOT DROPPED ON MY PLAYER
	var card = app.stage.getChildByName(data.id);

	//ADD RED BORER IF LOCKED, ADD BLUE IF MY PLAYER
	var isMyCard = socket.id === data.playerId;
	var color = isMyCard ? "0x0000ff" : "0xff0000";

	//SET TO NOTHING FIRST
	var borderToRemove = card.getChildByName("border_" + card.name);
	card.removeChildren();

	if (data.shouldLock) {
		var border = new Graphics();
		border.name = "border_" + card.name;
		border.lineStyle(6, color);
		border.drawRect(
			-card.width / 2 - 3,
			-card.height / 2 - 3,
			card.width + 6,
			card.height + 6
		);
		border.endFill();
		border.pivot.x = 0;
		border.pivot.y = 0;
		card.addChild(border);

		// IF FACE UP WHEN LOCKING, FLIP
		if (!card.flipped) {
			currentSprite = card;
			// var data = {
			// 	flipped: card.flipped,
			// 	id: card.name
			// };
			// socket.emit("itemClicked", data);
			animationId = requestAnimationFrame(flipToBack);
		}

		// if (!isMyCard) {
		// 	card.locked = true;

		// 	//IF FACE UP WHEN LOCKING, FLIP
		// 	if (!card.flipped) {
		// 		currentSprite = card;
		// 		animationId = requestAnimationFrame(flipToBack);
		// 	}
		// }
	} 
	else {
		card.locked = false;
	}
}

function hitTestRectangle(r1, r2) {
	// Define the variables we'll need to calculate
	let hit, combinedHalfWidths, combinedHalfHeights, vx, vy;

	// hit will determine whether there's a collision
	hit = false;

	// Find the center points of each sprite
	r1.centerX = r1.x + r1.width / 2;
	r1.centerY = r1.y + r1.height / 2;
	r2.centerX = r2.x + r2.width / 2;
	r2.centerY = r2.y + r2.height / 2;

	// Find the half-widths and half-heights of each sprite
	r1.halfWidth = r1.width / 2;
	r1.halfHeight = r1.height / 2;
	r2.halfWidth = r2.width / 2;
	r2.halfHeight = r2.height / 2;

	// Calculate the distance vector between the sprites
	vx = r1.centerX - r2.centerX;
	vy = r1.centerY - r2.centerY;

	// Figure out the combined half-widths and half-heights
	combinedHalfWidths = r1.halfWidth + r2.halfWidth;
	combinedHalfHeights = r1.halfHeight + r2.halfHeight;

	// Check for a collision on the x axis
	if (Math.abs(vx) < combinedHalfWidths) {
		// A collision might be occuring. Check for a collision on the y axis
		if (Math.abs(vy) < combinedHalfHeights) {
			// There's definitely a collision happening
			hit = true;
		} else {
			// There's no collision on the y axis
			hit = false;
		}
	} else {
		// There's no collision on the x axis
		hit = false;
	}

	// `hit` will be either `true` or `false`
	return hit;
}

function createClickListener(target) {
	var deltaXY;

	target.on("mousedown", onDragStart);
	target.on("touchstart", onDragStart);
	target.on("mouseup", onDragEnd);
	target.on("mouseupoutside", onDragEnd);
	target.on("touchend", onDragEnd);
	target.on("touchendoutside", onDragEnd);

	function onDragStart(event) {
		if (this.locked) return;
		deltaXY = this.data.getLocalPosition(this.parent);
	}

	function onDragEnd(event) {
		if (this.locked) return;
		this.data = event.data;
		var newPosition = this.data.getLocalPosition(this.parent);

		// DETECT CLICK
		var clicked =
			Math.abs(newPosition.x - deltaXY.x) < 1 ||
			Math.abs(newPosition.y - deltaXY.y) < 1;
		if (clicked) {
			currentSprite = target;
			
			var flipped = currentSprite.flipped;
			// debugger;

			// BROADCAST OTHERS TO FLIP
			var data = {
				flipped: !currentSprite.flipped,
				id: currentSprite.name
			};
			socket.emit("itemClicked", data);

			// FLIP HERE
			animationId = currentSprite.flipped
				? requestAnimationFrame(flipToFront)
				: requestAnimationFrame(flipToBack);
		}
	}
}

function itemClicked(data) {
	currentSprite = app.stage.getChildByName(data.id);
	if (currentSprite.locked) return;
	data.flipped
		? requestAnimationFrame(flipToBack)
		: requestAnimationFrame(flipToFront);
}

// CARDS MOVED
function movingItem(data) {
	var spriteToMove = app.stage.getChildByName(data.id);
	spriteToMove.x = data.coordinate.x;
	spriteToMove.y = data.coordinate.y;
}
