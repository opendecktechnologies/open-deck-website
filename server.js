// CREATE DECK
var offsetX = 0;
var	offsetY = 0;
var	width = 47.69;
var	height = 64;
var	unshuffledDeck = [];
var	shuffledDeck = [];
var players = [];
var firstLoad = true;

// ITERATE THROUGH CARD IMAGE COORDS IN TILESET
for (var i = 0; i < 52; i++) {
	offsetX += width;
	if (i % 13 === 0) {
		offsetX = 0;
		offsetY += height;
	}
	if (i === 0) {
		offsetX = 0;
		offsetY = 0;
	}

	var coordinates = {
		offsetX: offsetX,
		offsetY: offsetY
	};

	unshuffledDeck.push(coordinates);
}

// SHUFFLE DECK
for (var i = unshuffledDeck.length - 1; i >= 0; i--) {
	var randomPick = Math.floor(Math.random() * unshuffledDeck.length);
	var card = unshuffledDeck[randomPick];
	shuffledDeck.push(card);
	unshuffledDeck.splice(randomPick, 1);
}

// ALLOW SERVER TO TRACK POSITIONS
shuffledDeck.forEach(function(card, index){
	card.id = index;
	card.xAxisValue = index;
	card.yAxisValue = index;
	card.flipped = true;
});

// SERVER SETUP
var express = require("express");
var app = express();
var port = process.env.PORT || 5000;
var server = app.listen(port);
app.use(express.static("public"));

// SETUP ERROR LOGGING
var raygun = require("raygun");
var raygunClient = new raygun.Client().init({
	apiKey: "3/6T4IN5jp5iqdWWBDNTdQ=="
});
app.use(raygunClient.expressHandler);

try {
	// THE SYNCHRONOUS CODE WE WANT TO CATCH ERRORS ON
	var err = new Error("warp drive failing");
	throw err;
} catch (err) {
	raygunClient.send(err);
}

// SETUP SOCKET
var socket = require("socket.io");
var io = socket(server);
console.log("Server is running on port: " + port);

// LISTEN FOR NEW CONNECTIONS
io.sockets.on("connection", newConnection);

// PERFORM THIS ON NEW CONNECTIONS
function newConnection(socket) {
	// CENTER CARDS ON FIRST LOAD
	if (firstLoad) {
		var deckOffsetX = (socket.handshake.query.windowwidth / 2) - (unshuffledDeck.length / 2);
		var deckOffsetY = (socket.handshake.query.windowheight / 2) - (height / 2) - (unshuffledDeck.length / 2);

		shuffledDeck.forEach(function(card){
			card.xAxisValue += deckOffsetX;
			card.yAxisValue += deckOffsetY;
		})

		firstLoad = false;
	}

	// ATTACH THESE LISTENERS WHEN NEW CONNECTION MADE
	socket.on("itemMoved", itemMoved);
	socket.on("itemClicked", itemClicked);
	socket.on("disconnect", disconnected);
	socket.on("androidMessage", receivedAndroidMessage);
	socket.on("setCardLocked", setCardLocked);

	// ADD PLAYER TO MANAGED LIST OF PLAYERS POSITIONS
	var newPlayer = {
		id: socket.id,
		xAxisValue: 75 + (100 * players.length),
		yAxisValue: 75,
	};
	players.push(newPlayer);

	// PREPARE SOCKET INFO FOR OTHER CLIENTS
	var sockets = Object.keys(io.engine.clients);
	var connectionData = {
		socketId: socket.id,
		sockets: sockets,
		deck: shuffledDeck,
		players: players
	};

	// ADD PLAYER TO ALL OTHER SOCKETS
	io.sockets.emit("addPlayers", connectionData);

	/**
	 * LISTNER FUNCTIONS
	 */

	function itemMoved(data) {

		// UPDATE CARDS POSITION STATE
		if (data.id.indexOf("card_") !== -1) {
			shuffledDeck.forEach(function(card) {
				if (card.id == data.id.split('card_')[1]) {
					card.xAxisValue = data.coordinate.x;
					card.yAxisValue = data.coordinate.y;
				};
			});
		}

		// UPDATE PLAYERS POSITION STATE
		if (data.id.indexOf("player_container_") !== -1) {
			players.forEach(function(player) {
				if (player.id == data.id.split('player_container_')[1]) {
					player.xAxisValue = data.coordinate.x;
					player.yAxisValue = data.coordinate.y;
				};
			});
		}

		console.log('Players... ', players);

		// SIMPLY PASS INCOMING POSITION TO ALL SOCKETS
		socket.broadcast.emit("itemMoved", data);
	}

	function itemClicked(data) {
		// UPDATE FLIPPED STATUS FOR NEW CONNECTIONS
		shuffledDeck.forEach(function (card) {
			if (card.id == data.id.split('card_')[1]) {
				card.flipped = data.flipped;
			};
		});

		// SIMPLY PASS INCOMING POSITION TO ALL SOCKETS
		socket.broadcast.emit("itemClicked", data);
	}

	function disconnected(data) {
		console.log("incoming data disconnect", connectionData.socketId);
		console.log("players before disconnect", players);

		// UPDATE LIST OF PLAYERS
		var playerIndex;
		players.forEach(function(player, index){
			if (player.id == connectionData.socketId) {
				playerIndex = index;
			}
		});
		players.splice(playerIndex, 1);

		console.log("players after disconnect", players);

		socket.broadcast.emit("clientDisconnected", connectionData);
	}

	function receivedAndroidMessage(data) {
		console.log("Android message received!");
	}

	function setCardLocked(data) {
		io.sockets.emit("setCardLocked", data);
	}
}
